package com.example.eventlocation;


import java.util.regex.Pattern;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class LoginEvents extends Activity{
	Spinner spinner_country;
	String str_country_code;
	EditText ed_country_code,ed_cell_no,ed_name;
	Button b_login;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.loginevents);
		ed_country_code = (EditText)findViewById(R.id.edit_country_code);
		ed_cell_no = (EditText)findViewById(R.id.edit_cell_no);
		b_login = (Button)findViewById(R.id.but_login);
		
//		spinner functionality....
		spinner_country = (Spinner) findViewById(R.id.spinner_country);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
				this, R.array.CountryCodes,
				android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner_country.setAdapter(adapter);

		spinner_country.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> a, View v, int pos,
					long id) {
				// TODO Auto-generated method stub
				((TextView) a.getChildAt(0)).setTextColor(Color.BLACK);
				str_country_code = (String) spinner_country.getSelectedItem().toString();
				String name[] = str_country_code.split(Pattern.quote("("));
				for (int i = 0; i < name.length; i++) {
					String scountry = name[i];
					Log.i("scc", "" + scountry);
					String sa[] = scountry.split(Pattern.quote(")"));
					Log.i("scc", "" + sa);
					for (int j = 0; j < sa.length; j++) {
						ed_country_code.setText(sa[j]);
					}
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
		
		b_login.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent  ilogin = new Intent(LoginEvents.this,MainMenu.class);//moving to mainmenu class...
				startActivity(ilogin);
			}
		});
	}

}
