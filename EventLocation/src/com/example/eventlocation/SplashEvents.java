package com.example.eventlocation;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

public class SplashEvents extends Activity {
	Thread th;
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash_events);// activity_splash_events indicate view page in mobile

		th = new Thread(){
			@Override
			public void run() {
				// TODO Auto-generated method stub
				super.run();
				try {
					int time =0;
					while (time < 300) {
						sleep(1000);
						time = time+100;
					}
					finish();
				} catch (Exception e) {
					// TODO: handle exception
				}finally{
//					moving to login page....
					Intent in = new Intent(SplashEvents.this,LoginEvents.class);
					startActivity(in);
				}
			}
		};
		th.start();
	}
}
