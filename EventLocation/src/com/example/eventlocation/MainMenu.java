package com.example.eventlocation;


import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.Toast;

public class MainMenu extends Activity implements OnClickListener {
	Button b_aroundme, b_events, b_emergecy;
	AnimationDrawable animation;
	GPSTracker gps;// to get current latitude and longitude..........
	double current_latitude, current_longitude;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mainmenu);
		b_aroundme = (Button) findViewById(R.id.but_aroundme);
		b_events = (Button) findViewById(R.id.but_events);
		b_emergecy = (Button) findViewById(R.id.but_emergency);

		// for Animation buttons...
		TranslateAnimation anim_aroundme = new TranslateAnimation(1325, 0, 0, 0);
		anim_aroundme.setDuration(1000);
		anim_aroundme.setFillAfter(true);
		b_aroundme.startAnimation(anim_aroundme);
		
		TranslateAnimation anim_events = new TranslateAnimation(1325, 0, 0, 0);
		anim_events.setDuration(1500);
		anim_events.setFillAfter(true);
		b_events.startAnimation(anim_events);
		
		TranslateAnimation anim_emergency = new TranslateAnimation(1325, 0, 0, 0);
		anim_emergency.setDuration(2000);
		anim_emergency.setFillAfter(true);
		b_emergecy.startAnimation(anim_emergency);
		
		b_aroundme.setOnClickListener(this);
		b_events.setOnClickListener(this);
		b_emergecy.setOnClickListener(this);
		
		
		gps = new GPSTracker(MainMenu.this);
		if (gps.cangetLocation()) {

			current_latitude = gps.getLatitude();
			current_longitude = gps.getLongitude();
			System.out.println("Your Location is - \nLat: " + current_latitude
					+ "\nLong: " + current_longitude);
		} else {
			gps.showSettingsAlert();
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.but_aroundme:
//			checking for current latitude and longitude...
			if (current_latitude == 0.0 && current_longitude == 0.0) {
				Toast.makeText(getApplicationContext(), "searching for location"+"\n" + "Expose your mobile to open sky", Toast.LENGTH_LONG).show();
			} else {

			startActivity(new Intent(MainMenu.this,AroundMe.class));
			}
			break;
		case R.id.but_events:

			break;
		case R.id.but_emergency:

			break;

		default:
			break;
		}
	}

}
